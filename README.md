# errors
This library is a drop in replacement for the default [Go](https://golang.org) error implementation with a feature to nest
errors and provide call stack traces.

[![License](https://img.shields.io/badge/license-apache%20v2.0-blue.svg?style=flat-square)](https://opensource.org/licenses/Apache-2.0)
[![GoDoc](https://img.shields.io/badge/godoc-reference-blue.svg?style=flat-square)](https://godoc.org/bitbucket.org/azaher/errors)
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/azaher/errors.svg?style=flat-square)](https://bitbucket.org/azaher/errors/addon/pipelines/home)
[![Codecov](https://img.shields.io/codecov/c/bitbucket/azaher/errors.svg?style=flat-square)](https://codecov.io/bb/azaher/errors)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/azaher/errors?style=flat-square)](https://goreportcard.com/report/bitbucket.org/azaher/errors)

## Documentation
Please refer to the [godoc](https://godoc.org/bitbucket.org/azaher/errors) pages for documentation.

## Versioning
This project release version format follows [Semantic Versioning](http://semver.org/).

## Contributing
Pull requests and issue reports are welcomed.

## License
This project is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
